package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;


public class Asteroid {


	private static Vector2[] tva;
	private static Vector2 tv,temp1,temp2;
	private static Vector3 temp31,temp32;
	public static int[] apd;  //asteroids per destruir
	public static int apdi;
	private static float cr,ca,da;
	private static PolygonShape       tempBoxPoly;
	private static BodyDef tempBoxBodyDef;
	public static Asteroid[] asteroids;
	public static Texture tex,texDoodle;
	public static final int npunts = 8;
	private static final int nAsteroids = 100;

	private static float temp;
	private static int tempInt;

	private static int nActiveAsteroids=0;


	private static float Tmass,Omass;
	private static int[] candidates;
	private static int nCandidates;




	private static float dtSpawner,dtWave,dtField;
	private static enum states {NORMAL,WAVE,FIELD};
	private static states state;


	public static ParticleEffect pe;
	private static int mindex;

	private static int ti;
	private static final float blurR=.8f;
	private static final int blurInt=60;





	public float size,life;
	public float[] radis;
	public boolean visible;
	public Body cos;
	public Mesh mesh;
	public float[] meshFloat;
	private boolean dins,field;
	private Mesh meshLines;
	private float[] meshLinesFloat;

	private float[] meshGlowFloat;
	private Mesh meshGlow;
	private short[] meshGlowIndex;


	public static void ini(){

		dtSpawner=99;   //aixi ja comen�a
		dtWave=0;
		state=states.NORMAL;



		mindex=0;
		candidates = new int[10];


		temp1=new Vector2();
		temp2=new Vector2();


		temp31=new Vector3();
		temp32=new Vector3();


		pe=new ParticleEffect();
		pe.loadEmitters(Gdx.files.internal("data/particle"));
		pe.loadEmitterImages(Gdx.files.internal("data/"));
		//pe.start();

		tex= new Texture(Gdx.files.internal("data/img/asteroid.png"));
		texDoodle= new Texture(Gdx.files.internal("data/img/asteroid_doodle.png"));



		tempBoxPoly = new PolygonShape();
		tempBoxBodyDef = new BodyDef();


		tva = new Vector2[npunts];
		for (int i = 0; i < tva.length; i++) {
			tva[i] = new Vector2();
		}
		tv=new Vector2();

		apdi=0;
		apd=new int[nAsteroids];
		for (int i = 0; i < apd.length; i++) {
			apd[i]=999;
		}



	}


	public static void create_asteroids(){
		asteroids = new Asteroid[nAsteroids];

		for (int i = 0; i < asteroids.length; i++) {
			asteroids[i] = new Asteroid();
			asteroids[i].create(.8f+(float)Math.random()*1.7f, new Vector2(0,0), new Vector2(0,0), i);
			//asteroids[i].create(.6f+(float)Math.random()*1.5f);
		}
	}


	public Asteroid(){


		dins=false;
		field=false;
		size=0;
		visible=false;

		radis=new float[npunts];

		meshFloat = new float[(npunts+2)*6];
		mesh = new Mesh(true, npunts+2, npunts+2, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"),
				new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));



		meshLinesFloat = new float[(npunts)*2*4];
		meshLines = new Mesh(true, (npunts)*2, (npunts)*2, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));


		

		meshGlowFloat = new float[(npunts)*3*4];
		//meshGlowIndex=new short[(npunts+1)*3];
		meshGlowIndex=new short[]{0,8,1,9,2,10,3,11,4,12,5,13,6,14,7,15,0,8,8,8,0,0,16,1,17,2,18,3,19,4,20,5,21,6,22,7,23,0,16};

		meshGlow = new Mesh(true, meshGlowIndex.length, meshGlowIndex.length, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));



		
	}
	public void create (float isize,Vector2 pos, Vector2 vel, int index){
		create(isize,pos, vel, index,true);
	}

	public void create (float isize,Vector2 pos, Vector2 vel, int index,boolean real){



		visible=false;
		size=isize;




		create_meshTex(real);
		create_meshGlowT();





		mesh.setVertices(meshFloat);
		meshLines.setVertices(meshLinesFloat);
		meshGlow.setVertices(meshGlowFloat);
		meshGlow.setIndices(meshGlowIndex);
		

		if(real){

			tempBoxPoly.set(tva);

			tempBoxBodyDef.type = BodyType.DynamicBody;
			tempBoxBodyDef.position.set(pos);
			tempBoxBodyDef.linearVelocity.set(vel);
			tempBoxBodyDef.active=false;

			tempBoxBodyDef.angularVelocity=-1+6*(float)Math.random();




			cos = Box.world.createBody(tempBoxBodyDef);
			cos.createFixture(tempBoxPoly, 4);
			cos.setUserData(index);  //guardem l'index del asteroid

		}


	}

	private void create_meshGlowT(){
		ca=0;
		da=((2*(float)Math.PI)/(npunts));
		for (int j = 0; j <npunts ; j++) {
			ca=da*j;



			ti=((j*2)*4);

			meshLinesFloat[ti]=meshFloat[((j+1)*6)];
			meshLinesFloat[ti+1]=meshFloat[((j+1)*6)+1];
			meshLinesFloat[ti+2]=0;
			meshLinesFloat[ti+3]=Color.toFloatBits(255, 255, 255, 255);


			ti=(((j*2)+1)*4);

			meshLinesFloat[ti]=meshFloat[((j+2)*6)];
			meshLinesFloat[ti+1]=meshFloat[((j+2)*6)+1];
			meshLinesFloat[ti+2]=0;
			meshLinesFloat[ti+3]=Color.toFloatBits(255, 255, 255, 255);



			meshGlowFloat[(((j))*4)] = meshFloat[((j+1)*6)];
			meshGlowFloat[(((j))*4)+1] = meshFloat[((j+1)*6)+1];
			meshGlowFloat[(((j))*4)+2] = 0;
			meshGlowFloat[(((j))*4)+3] = Color.toFloatBits(255, 255, 255, blurInt);


			meshGlowFloat[(((j)+(npunts))*4)] = meshGlowFloat[(((j))*4)]+(float)Math.cos(ca)*blurR*(float)Math.sqrt(radis[j]);
			meshGlowFloat[(((j)+(npunts))*4)+1] = meshGlowFloat[(((j))*4)+1]+(float)Math.sin(ca)*blurR*(float)Math.sqrt(radis[j]);
			meshGlowFloat[(((j)+(npunts))*4)+2] = 0;
			meshGlowFloat[(((j)+(npunts))*4)+3] = Color.toFloatBits(255, 255, 255, 0);


			meshGlowFloat[(((j)+(npunts)*2)*4)] = meshGlowFloat[(((j))*4)]-(float)Math.cos(ca)*blurR*(float)Math.sqrt(radis[j]);
			meshGlowFloat[(((j)+(npunts)*2)*4)+1] = meshGlowFloat[(((j))*4)+1]-(float)Math.sin(ca)*blurR*(float)Math.sqrt(radis[j]);
			meshGlowFloat[(((j)+(npunts)*2)*4)+2] = 0;
			meshGlowFloat[(((j)+(npunts)*2)*4)+3] = Color.toFloatBits(255, 255, 255, 0);


		}

	}




	private void create_meshTex(boolean real) {


		cr=size;
		//npunts=(int)((Math.PI*2*size)/.2f);
		//if(npunts>8)npunts=8;
		ca=0;
		da=((2*(float)Math.PI)/(npunts));

		for (int j = 0; j <npunts ; j++) {
			if(real){
				cr=cr+size*(-.4f+(float)Math.random()*.8f);
			}else{
				
			}
			if(cr<.6f)cr=.6f;
			ca=da*j;
			tva[j].set(cr*(float)Math.cos(ca), cr*(float)Math.sin(ca));
			radis[j]=cr;


			meshFloat[((j+1)*6)] = tva[j].x;
			meshFloat[((j+1)*6)+1] = tva[j].y;
			meshFloat[((j+1)*6)+2] = 0;
			meshFloat[((j+1)*6)+3] = Color.toFloatBits(255, 255, 255, 255);
			//meshFloat[((j+1)*6)+4] = .5f+(((float)Math.cos(ca))*size/(6));
			//meshFloat[((j+1)*6)+5] = .5f+(((float)Math.sin(ca))*size/(6));

			meshFloat[((j+1)*6)+4] = .5f+(float)Math.cos(ca)*0.3965f+(float)Math.cos(ca)*0.1035f*(.6f/cr);
			meshFloat[((j+1)*6)+5] = .5f+(float)Math.sin(ca)*0.3965f+(float)Math.sin(ca)*0.1035f*(.6f/cr);








		}



		meshFloat[0] = 0;
		meshFloat[1] = 0;
		meshFloat[2] = 0;
		meshFloat[3] = Color.toFloatBits(255, 255, 255, 255);
		meshFloat[4] = .5f;
		meshFloat[5] = .5f;

		meshFloat[(npunts+1)*6+0] = meshFloat[(1)*6+0];
		meshFloat[(npunts+1)*6+1] = meshFloat[(1)*6+1];;
		meshFloat[(npunts+1)*6+2] = meshFloat[(1)*6+2];;
		meshFloat[(npunts+1)*6+3] = meshFloat[(1)*6+3];;
		meshFloat[(npunts+1)*6+4] = meshFloat[(1)*6+4];;
		meshFloat[(npunts+1)*6+5] = meshFloat[(1)*6+5];;

	}






	public static void spawner(float dt) {

		switch (state) {
		case NORMAL:
			dtSpawner+=dt;
			if(dtSpawner>=Math.pow((1.0/Main.gameScr.totalTime),.5)*25.0){
				spawn();
				dtSpawner=0;


				if(dtField>=150){
					state=states.FIELD;
					dtField=0;
					dtSpawner=0;


					//convertim tots els asteroids que hi hagin en field
					//					for (int i = 0; i < Asteroid.asteroids.length; i++) {
					//						if(Asteroid.asteroids[i].visible){
					//							Asteroid.asteroids[i].field=true;
					//
					//						}
					//					}

				}


			}


			dtField+=dt;



			break;


		case FIELD:

			dtSpawner+=dt;
			dtField+=dt;

			if(dtSpawner>=Math.pow((1.0/Main.gameScr.totalTime),.3)*2f){
				spawnField();
				dtSpawner=0;
				if(dtField>9){
					dtField=0;
					state=states.NORMAL;
				}
			}


			break;


		}


	}



	public static void spawn(float isize, float posx,float posy, float velx,float vely) {
		spawn(new Vector2(posx,posy), new Vector2(velx,vely));
	}


	public static void spawnField() {


		temp1.x=(-Box.camera.viewportWidth/2.0f)+Box.camera.viewportWidth*(float)Math.random();
		temp1.y=Box.camera.viewportHeight;

		temp2.y=-((float)Math.random()*4+8);
		temp2.x=-.5f+(float)Math.random();

		spawn(temp1,temp2,false);
	}



	public static void spawn() {

		if(Math.random()>.5){
			if(Math.random()>.5){
				temp1.x=Box.camera.viewportWidth;
				temp1.y=Box.camera.viewportHeight/4.0f*(float)Math.random();

				temp2.x=-3-(float)Math.random()*6;
				temp2.y=-temp1.y/7;
			}else{
				temp1.x=-Box.camera.viewportWidth;
				temp1.y=-Box.camera.viewportHeight/4.0f*(float)Math.random();

				temp2.x=3+(float)Math.random()*6;
				temp2.y=-temp1.y/7;
			}
		}else{
			if(Math.random()>.5){
				temp1.x=Box.camera.viewportWidth/4.0f*(float)Math.random();
				temp1.y=Box.camera.viewportHeight;

				temp2.y=-3-(float)Math.random()*6;
				temp2.x=-temp1.x/7;
			}else{
				temp1.x=-Box.camera.viewportWidth/4.0f*(float)Math.random();
				temp1.y=-Box.camera.viewportHeight;

				temp2.y=3+(float)Math.random()*6;
				temp2.x=-temp1.x/7;

			}
		}



		spawn(temp1,temp2);
	}

	public static void spawn(Vector2 pos, Vector2 vel){
		spawn(pos, vel,false);
	}

	public static void spawn(Vector2 pos, Vector2 vel,boolean ifield) {
		tempInt=(int)(Math.random()*(asteroids.length-2.0f));

		
		for (int i = tempInt; i < asteroids.length; i++) {
			if(!asteroids[i].visible){
				asteroids[i].ns_spawn(pos, vel,ifield);
				return;
			}
		}

		
		for (int i = 0; i < tempInt; i++) {
			if(!asteroids[i].visible){
				asteroids[i].ns_spawn(pos, vel,ifield);
				return;
			}
		}




	}

	public void ns_spawn(Vector2 pos, Vector2 vel){	
		ns_spawn(pos,vel,false);
	}

	public void ns_spawn(Vector2 pos, Vector2 vel,boolean ifield){
		cos.setLinearVelocity(vel);
		cos.setTransform(pos, 0);
		visible=true;
		dins=false;
		life=0;
		cos.setActive(true);
		field=ifield;
	}

	public static void spawnIndex(Vector2 pos, Vector2 vel,int index) {

		spawnIndex(pos, vel,index,false);

	}


	public static void spawnIndex(Vector2 pos, Vector2 vel,int index,boolean ifield) {

		asteroids[index].ns_spawn(pos, vel,ifield);

	}



	public static void checkBound() {

		for (int i = 0; i < asteroids.length; i++) {
			if(asteroids[i].visible && !asteroids[i].field){
				if(asteroids[i].dins){
					if(asteroids[i].cos.getPosition().x>Box.camera.viewportWidth/2){
						asteroids[i].cos.setTransform(-Box.camera.viewportWidth/2, asteroids[i].cos.getPosition().y, asteroids[i].cos.getAngle());
					}else if(asteroids[i].cos.getPosition().x<-Box.camera.viewportWidth/2){
						asteroids[i].cos.setTransform(Box.camera.viewportWidth/2, asteroids[i].cos.getPosition().y, asteroids[i].cos.getAngle());
					}

					if(asteroids[i].cos.getPosition().y>Box.camera.viewportHeight/2){
						asteroids[i].cos.setTransform(asteroids[i].cos.getPosition().x, -Box.camera.viewportHeight/2, asteroids[i].cos.getAngle());
					}else if(asteroids[i].cos.getPosition().y<-Box.camera.viewportHeight/2){
						asteroids[i].cos.setTransform(asteroids[i].cos.getPosition().x, Box.camera.viewportHeight/2, asteroids[i].cos.getAngle());
					}
				}else{

					if(asteroids[i].cos.getPosition().x>Box.camera.viewportWidth/2.5){
					}else if(asteroids[i].cos.getPosition().x<-Box.camera.viewportWidth/2.5){
					}else if(asteroids[i].cos.getPosition().y>Box.camera.viewportHeight/2.5){
					}else if(asteroids[i].cos.getPosition().y<-Box.camera.viewportHeight/2.5){
					}else{
						asteroids[i].dins=true;
					}

				}
			}

		}


	}

	public static void addDestroy(int index) {

		apd[apdi]=index;
		apdi++;

	}



	public static void checkDestroy() {



		for (int i = 0; i < apdi; i++) {

			if(asteroids[apd[i]].visible==true){



				//posssibilitat de powerup




				//			if(asteroids[apd[i]].cos.getMass()>40){
				//				for (int j = 0; j < 3; j++) {
				//					spawn(asteroids[apd[i]].size/1.5f,asteroids[apd[i]].cos.getPosition(),asteroids[apd[i]].cos.getLinearVelocity().add(-2+4*(float)Math.random(), -2+4*(float)Math.random()));
				//				}
				//			}else if(asteroids[apd[i]].cos.getMass()>17){
				//				for (int j = 0; j < 2; j++) {
				//					spawn(asteroids[apd[i]].size/1.5f,asteroids[apd[i]].cos.getPosition(),asteroids[apd[i]].cos.getLinearVelocity().add(-2+4*(float)Math.random(), -2+4*(float)Math.random()));
				//				}
				//			}

				//search 3 asteroids

				nCandidates=0;
				Omass=asteroids[apd[i]].cos.getMass();

				Tmass=0;
				if(Omass>=17){
					primerI:
						for (int j = 0; j < asteroids.length; j++) {
							if(asteroids[j].visible==false && asteroids[j].cos.getMass()<(Omass/2)){
								candidates[nCandidates]=j;
								nCandidates++;
								Tmass+=asteroids[j].cos.getMass();


								break primerI;
							}
						}


				for (int k = 0; k < 5; k++) {
					find:
						for (int j = (int)(asteroids.length*Math.random()); j < asteroids.length; j++) {
							if(asteroids[j].visible==false && (asteroids[j].cos.getMass()+Tmass)<(Omass*0.8f)){
								candidates[nCandidates]=j;
								nCandidates++;
								Tmass+=asteroids[j].cos.getMass();

								break find;
							}
						}

				}


				Tmass=0;

				for (int j = 0; j < nCandidates; j++) {

					//TODO que la velocitat dels asteroides residus sigui en la direcció de la bala
					spawnIndex(asteroids[apd[i]].cos.getPosition(),asteroids[apd[i]].cos.getLinearVelocity().add(-5+10*(float)Math.random(), -5+10*(float)Math.random()),candidates[j],asteroids[apd[i]].field);
					asteroids[candidates[j]].dins=true;  //els asteroids que provenen d'una explosio els podem considerar que estan dins.

				}


				}else{
					if(Math.random()>.96){
						PowerUp.spawn_one(asteroids[apd[i]].cos.getPosition(), asteroids[apd[i]].cos.getLinearVelocity());
					}
				}

				nCandidates=0;


				asteroids[apd[i]].destroy();

			}
		}


		apdi=0;

	}


	public void destroy(){
		temp31.set(cos.getPosition().x, cos.getPosition().y, 0);
		Box.camera.project(temp31);

		Explosion.spawn_one(cos.getPosition(),(float)Math.sqrt(cos.getMass()/25.0f));
		Asteroid.pe.setPosition(temp31.x, temp31.y);
		Asteroid.pe.getEmitters().get(0).getVelocity().setHighMax( (float)Math.sqrt(cos.getMass()*30000) );
		Asteroid.pe.getEmitters().get(0).getVelocity().setHighMin((float)Math.sqrt(cos.getMass()*4000));
		Asteroid.pe.start();


		visible=false;
		cos.setActive(false);

		Main.gameScr.points+=Main.gameScr.points*0.01f;
	}


	public void set_Floats(){



		for (int i = 0; i < meshFloat.length; i=i+6) {




		}

	}

	public void a_draw_tex(){
		mesh.render(GL11.GL_TRIANGLE_FAN, 0, Asteroid.npunts+2);

	}


	private void a_draw_doodle(){
		
		mesh.render(GL11.GL_TRIANGLE_FAN, 0, Asteroid.npunts+2);

	}
	
	public void a_draw_glow(){


		Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);


		meshLines.render(GL11.GL_LINES, 0, (npunts)*2);


		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);

		meshGlow.render(GL11.GL_TRIANGLE_STRIP, 0, meshGlowIndex.length);



	}


	private void a_draw(){

		switch (Global.estil) {
		case TEX:
			a_draw_tex();
			break;

		case GLOW:
			a_draw_glow();
			break;
			
		case DOODLE:
			a_draw_doodle();
			break;

		}

	}


	public static void upd_all(float dt){

		tempInt=0;
		for (int i = 0; i < Asteroid.asteroids.length; i++) {
			if(Asteroid.asteroids[i].visible){
				Asteroid.asteroids[i].upd(dt);
				tempInt++;

			}
		}
		nActiveAsteroids=tempInt;
		tempInt=0;

	}

	public void upd(float dt){
		life+=dt;

		if(!dins && life>=20){ // si en 20 segons no ha aparegut a la pantalla, s'elimina
			visible=false;
			cos.setActive(false);
		}
	}



	public void draw(){


		Gdx.graphics.getGL11().glPushMatrix();


		Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y, 0);
		Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);

		a_draw();


		Gdx.graphics.getGL11().glPopMatrix();



		if(dins&& !field){

			//mirror
			if(cos.getPosition().y>Box.camera.viewportHeight*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y-Box.camera.viewportHeight, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				a_draw();
				Gdx.graphics.getGL11().glPopMatrix();
			}

			if(cos.getPosition().y<-Box.camera.viewportHeight*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y+Box.camera.viewportHeight, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				a_draw();
				Gdx.graphics.getGL11().glPopMatrix();
			}


			if(cos.getPosition().x>Box.camera.viewportWidth*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x-Box.camera.viewportWidth, cos.getPosition().y, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				a_draw();
				Gdx.graphics.getGL11().glPopMatrix();
			}

			if(cos.getPosition().x<-Box.camera.viewportWidth*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x+Box.camera.viewportWidth, cos.getPosition().y, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				a_draw();
				Gdx.graphics.getGL11().glPopMatrix();
			}
		}


	}


	public static void draw_all(){



		switch (Global.estil) {

		case TEX:
			Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			Asteroid.tex.bind();
			break;

		case GLOW:
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			break;
			
		case DOODLE:
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			Asteroid.texDoodle.bind();
			break;
		}


		//Draw Asteroids
		for (int i = 0; i < Asteroid.asteroids.length; i++) {
			if(Asteroid.asteroids[i].visible){
				Asteroid.asteroids[i].draw();

			}
		}


		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}


	public static boolean is_in_field(){
		return state==states.FIELD;
	}


}
