package com.gerardas.asteroidsDesktop;

import org.lwjgl.opengl.GL11;

import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Bullet {

	public static Bullet[] bullets = new Bullet[30];

	private static PolygonShape       tempBoxPoly;
	private static BodyDef tempBoxBodyDef;

	private static Mesh mesh;

	private static final int nBullets = 150;
	
	
	

	public boolean visible;
	public PointLight pl;

	public Body cos;
	public float TTL;


	public static void ini(){
		bullets = new Bullet[nBullets];

		tempBoxPoly = new PolygonShape();
		tempBoxBodyDef = new BodyDef();


		tempBoxPoly.set(new Vector2[] {
				new Vector2(-.1f, .5f),
				new Vector2(-.1f, -.5f),
				new Vector2(.1f, -.5f),
				new Vector2(.1f, .5f),
		});


		for (int i = 0; i < bullets.length; i++) {
			bullets[i] = new Bullet();
		}

		tempBoxPoly.dispose();
		
		
		mesh = new Mesh(true, 2, 2, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		mesh.setVertices(new float[] { 
				0,  0f, 0, Color.toFloatBits(255, 255, 255, 255),
				0 , -1f, 0, Color.toFloatBits(255, 255, 255, 255) });
		
		
	}



	public Bullet(){
		visible=false;
		pl = new PointLight(Box.rayHandler, 5, new Color(.6f,.6f,1,.2f), 3, 0, 0);
		pl.setXray(true);
		pl.setActive(false);

		TTL=0;


		tempBoxBodyDef = new BodyDef();
		tempBoxBodyDef.type = BodyType.DynamicBody;
		tempBoxBodyDef.position.x = 0;
		tempBoxBodyDef.position.y = 0;
		//boxBodyDef.linearVelocity.set(-9+(float)Math.random()*18, -9+(float)Math.random()*18);
		tempBoxBodyDef.active=false;
		//tempBoxBodyDef.bullet=true;

		Body boxBody = Box.world.createBody(tempBoxBodyDef);
		boxBody.createFixture(tempBoxPoly, 10);
		boxBody.setUserData(0);
		cos=boxBody;

	}


	public void upd(float dt){
		TTL-=dt;
		if(TTL<=0)destroy();
		pl.setPosition(cos.getPosition());

		if(cos.getPosition().x>Box.camera.viewportWidth/1.95){
			cos.setTransform(-Box.camera.viewportWidth/1.95f, cos.getPosition().y, cos.getAngle());
		}else if(cos.getPosition().x<-Box.camera.viewportWidth/1.95){
			cos.setTransform(Box.camera.viewportWidth/1.95f, cos.getPosition().y, cos.getAngle());
		}

		if(cos.getPosition().y>Box.camera.viewportHeight/1.95){
			cos.setTransform(cos.getPosition().x, -Box.camera.viewportHeight/1.95f, cos.getAngle());
		}else if(cos.getPosition().y<-Box.camera.viewportHeight/1.95){
			cos.setTransform(cos.getPosition().x, Box.camera.viewportHeight/1.95f, cos.getAngle());
		}

	}



	public static void shoot(Vector2 pos, Vector2 vel, float ang) {

		findBullet:
			for (int i = 0; i < bullets.length; i++) {
				if(!bullets[i].cos.isActive()){


					bullets[i].cos.setTransform(pos, ang);
					bullets[i].cos.setLinearVelocity(vel.x+(Ship.cos.getLinearVelocity().x/4),vel.y+(Ship.cos.getLinearVelocity().y/4));
					bullets[i].cos.setActive(true);
					bullets[i].cos.setUserData(0);
					bullets[i].cos.setFixedRotation(true);
					bullets[i].visible=true;
					bullets[i].pl.setActive(true);
					bullets[i].TTL=1.4f;

					break findBullet;
				}
			}

	}



	public static void draw_all() {

		
		Gdx.graphics.getGL10().glDisable(GL10.GL_TEXTURE_2D);

		for (int i = 0; i < Bullet.bullets.length; i++) {

			if(Bullet.bullets[i].visible){
				Bullet.bullets[i].draw();
			}

		}

	}

	private void draw() {
		Gdx.graphics.getGL10().glPushMatrix();
		Gdx.graphics.getGL10().glTranslatef(cos.getPosition().x, cos.getPosition().y, 0);
		Gdx.graphics.getGL10().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);

//		IMR.begin(GL10.GL_LINES);
//		IMR.color(1, 1, 1, 1);
//		IMR.vertex(0,-.5f , 0);
//		IMR.color(1, 1, 1, 1);
//		IMR.vertex(0,.5f , 0);	
//		IMR.end();
		
		
		mesh.render(GL11.GL_LINES, 0, 2);
		
		Gdx.graphics.getGL10().glPopMatrix();
	}



	public static void upd_all(float dt) {

		//borrar bullets
		for (int i = 0; i < Bullet.bullets.length; i++) {

			if(Bullet.bullets[i].cos.getUserData().equals(1)){

				Bullet.bullets[i].destroy();


			}
		}



		for (int i = 0; i < bullets.length; i++) {
			if(bullets[i].visible)bullets[i].upd(dt);
		}

	}



	private void destroy() {
		cos.setActive(false);
		pl.setActive(false);
		visible=false;
		cos.setUserData(0);
		
	}

}
