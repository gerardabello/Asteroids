package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Gdx;


public class Global {


	public static float viewportW;
	public static float viewportH;
	
	public static enum estils{TEX,GLOW,DOODLE};
	public static estils estil;
	
	public static enum Controls{TILT,DPAD,ARROWS};
	public static Controls control;
	public static float scale;


	public static void ini(){
		Highscores.ini();
		estil=estils.TEX;
		control=Controls.ARROWS;

		viewportW=48;
		viewportH=48*((float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth());
		
		scale = Gdx.graphics.getHeight()/480f;
	}




	public static float box2batch_x(float x){

		return Gdx.graphics.getWidth()/2+x*(Gdx.graphics.getWidth()/viewportW);

	}

	public static float box2batch_y(float y){

		return Gdx.graphics.getHeight()/2+y*(Gdx.graphics.getHeight()/viewportH);

	}



	public static float batch2box_x(float x){

		return -viewportW/2+x/(Gdx.graphics.getWidth()/Box.camera.viewportWidth);

	}

	public static float batch2box_y(float y){

		return -viewportH/2+y/(Gdx.graphics.getHeight()/Box.camera.viewportHeight);

	}





}
