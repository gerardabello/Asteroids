package com.gerardas.asteroidsDesktop;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SplashScreen implements Screen {

	private SpriteBatch spriteBatch;
	private Texture grass;
	private Game myGame;
	private int count=0;

	/**
	 * Constructor for the splash screen
	 * @param g Game which called this splash screen.
	 */
	public SplashScreen(Game g)
	{
		myGame = g;
		spriteBatch = new SpriteBatch();
		grass = new Texture(Gdx.files.internal("data/img/grass.png"));

	}

	@Override
	public void render(float delta){

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();

		spriteBatch.disableBlending();

		for(int i = 0; i <= (Gdx.graphics.getWidth()/grass.getWidth()); i++) {
			for(int j = 0; j <= (Gdx.graphics.getHeight()/grass.getHeight()); j++) {
				spriteBatch.draw(grass,grass.getWidth()*i ,grass.getHeight()*j ); 
			}
		}

		spriteBatch.end();


		if(count==3){

			count=0;
			Main.doneSplash=true;
			if(Main.gameScr==null){
				Gdx.app.log("NEW", "GS");
				Main.mainMenuScr= new MainMenuScreen(myGame);
				Main.gameScr= new GameScreen(myGame);
				Main.Ip=new InputProc();
				myGame.setScreen(Main.mainMenuScr);
			}else{
				Main.gameScr.resume();
				myGame.setScreen(Main.gameScr);
			}

		}else{
			count++;
		}

	}

	@Override
	public void show(){

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}


}
