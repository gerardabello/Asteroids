package com.gerardas.asteroidsDesktop;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;

public class GameUI {
	
	
	private static Mesh meshHealth;
	private static float[] meshFloatHealth;
	private static Mesh meshHealthB;
	private static Mesh meshPad;
	private static Mesh meshThrust;
	private static Mesh meshArrows;
	
	
	public static final float scale=5;


	public static void ini(){
		
		ini_healthBar();
		
		ini_Pad();
		ini_Thrust();
		ini_Arrows();
	}
	
	
	








	public static void draw(){
		
		draw_health();
		
		
		
		switch (Global.control) {
		case TILT:
			
			break;
			
			
		case DPAD:
			draw_Pad();

			break;
			
			
		case ARROWS:
			draw_Arrows();

			break;

		}
		
		
		draw_Thrust();

	}
	
	
	
	
	private static void ini_Arrows() {
		meshArrows = new Mesh(true, 9, 9, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshArrows.setVertices(new float[]{
			0, 0,0,Color.toFloatBits(1f, 1f, 1f, .3f),
			1, 1,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			1, 0,0,Color.toFloatBits(1f, 1f, 1f, .3f),
			1, .3f,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			2, 0,0,Color.toFloatBits(1f, 1f, 1f, .3f),
			2, .3f,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			2, 1,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			3, 0,0,Color.toFloatBits(1f, 1f, 1f, .3f)
			
		});
		
		
		
		meshArrows.setIndices(new short[]{0,1,2,3,4,5,4,6,7});
		
	}

	
	private static void draw_Arrows() {
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glPushMatrix();
		Gdx.graphics.getGL11().glTranslatef(-Global.viewportW/2, -Global.viewportH/2, 0);
		Gdx.graphics.getGL11().glScalef(scale, scale, scale);
		
		
		meshArrows.render(GL11.GL_TRIANGLE_STRIP,0,meshArrows.getNumIndices());
				
		
		Gdx.graphics.getGL11().glPopMatrix();
		
		
	}

	
	

	private static void ini_Thrust() {
		meshThrust = new Mesh(true, 3, 3, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshThrust.setVertices(new float[]{
			0, 0,0,Color.toFloatBits(1f, 1f, 1f, .3f),
			-1, 0,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			0, 1,0,Color.toFloatBits(1f, 1f, 1f, .15f)
			
		});
		
	}

	
	private static void draw_Thrust() {
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glPushMatrix();
		Gdx.graphics.getGL11().glTranslatef(Global.viewportW/2, -Global.viewportH/2, 0);
		Gdx.graphics.getGL11().glScalef(scale, scale, scale);
		
		
		meshThrust.render(GL11.GL_TRIANGLES,0,3);
		
		
		
		Gdx.graphics.getGL11().glPopMatrix();
		
		
	}






	private static void ini_Pad() {

		meshPad = new Mesh(true, 3, 3, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshPad.setVertices(new float[]{
			0, 1,0,Color.toFloatBits(1f, 1f, 1f, .3f),
			-.866025f, -.5f,0,Color.toFloatBits(1f, 1f, 1f, .15f),
			.866025f, -.5f,0,Color.toFloatBits(1f, 1f, 1f, .15f)
			
		});
		
	}


	
	
	private static void draw_Pad() {
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glPushMatrix();
		Gdx.graphics.getGL11().glTranslatef(Input.padPosx, Input.padPosy, 0);
		Gdx.graphics.getGL11().glRotatef(Ship.cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
		Gdx.graphics.getGL11().glScalef(scale/2, scale/2, scale/2);
		Gdx.graphics.getGL11().glPointSize(5);
		
		meshPad.render(GL11.GL_TRIANGLES,0,3);
		
		Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
		meshPad.render(GL11.GL_POINTS,0,3);
		
		
		Gdx.graphics.getGL11().glPopMatrix();
		
		
	}




	private static void ini_healthBar(){
		meshFloatHealth =new float[4*4];

		meshHealth = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));

		
		meshFloatHealth[(0*4)+0]=-(Box.camera.viewportWidth/2)+1;
		meshFloatHealth[(0*4)+1]=(Box.camera.viewportHeight/2)-1;
		
		
		meshFloatHealth[(1*4)+0]=meshFloatHealth[(0*4)+0];
		meshFloatHealth[(1*4)+1]=meshFloatHealth[(0*4)+1]-1;
		
		
		meshFloatHealth[(2*4)+0]=meshFloatHealth[(1*4)+0]+1*8;
		meshFloatHealth[(2*4)+1]=meshFloatHealth[(1*4)+1];
		
		meshFloatHealth[(3*4)+0]=meshFloatHealth[(0*4)+0]+1*8;
		meshFloatHealth[(3*4)+1]=meshFloatHealth[(0*4)+1];
				
				
				
		meshFloatHealth[(0*4)+2]=0;
		meshFloatHealth[(1*4)+2]=0;		
		meshFloatHealth[(2*4)+2]=0;	
		meshFloatHealth[(3*4)+2]=0;		
		
		
		
		meshFloatHealth[(0*4)+3]=Color.toFloatBits(0.7f, .7f, 1f, .4f);
		meshFloatHealth[(1*4)+3]=meshFloatHealth[(0*4)+3];		
		meshFloatHealth[(2*4)+3]=meshFloatHealth[(0*4)+3];	
		meshFloatHealth[(3*4)+3]=meshFloatHealth[(0*4)+3];
		
		
		
		
		meshHealthB = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshHealthB.setVertices(meshFloatHealth);
		
		
		
				
		meshFloatHealth[(0*4)+3]=Color.toFloatBits(1f, 1f, 1f, .8f);
		meshFloatHealth[(1*4)+3]=meshFloatHealth[(0*4)+3];		
		meshFloatHealth[(2*4)+3]=meshFloatHealth[(0*4)+3];	
		meshFloatHealth[(3*4)+3]=meshFloatHealth[(0*4)+3];
		
		
	}



	private static void draw_health() {
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		
		
		meshFloatHealth[(2*4)+0]=meshFloatHealth[(1*4)+0]+Ship.health*8;
		meshFloatHealth[(2*4)+1]=meshFloatHealth[(1*4)+1];
		
		meshFloatHealth[(3*4)+0]=meshFloatHealth[(0*4)+0]+Ship.health*8;
		meshFloatHealth[(3*4)+1]=meshFloatHealth[(0*4)+1];
		

		
		
		meshHealth.setVertices(meshFloatHealth);
		
		meshHealthB.render(GL11.GL_TRIANGLE_FAN, 0, 4);
		meshHealth.render(GL11.GL_TRIANGLE_FAN, 0, 4);
		
		

		
		
	}

}
