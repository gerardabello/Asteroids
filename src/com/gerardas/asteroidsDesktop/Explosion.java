package com.gerardas.asteroidsDesktop;

import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class Explosion {
	
	public static Explosion[] explosions;
	public static final int nex=5;
	public static final float defTime=.3f;
	
	
	
	
	public float TTL,time;
	public float size;
	public PointLight pl1,pl2;
	
	
	public Explosion(){
		time=.3f;
		TTL=0;
		size=1;
		pl1= new PointLight(Box.rayHandler, 8, new Color(.8f,.8f,1,1), 5, 0, 0);
		pl1.setXray(true);
		pl1.setActive(false);
		pl1.setStaticLight(true);
		pl2= new PointLight(Box.rayHandler, 15, new Color(.4f,.4f,.4f,1), 12, 0, 0);
		pl2.setXray(false);
		pl2.setActive(false);
		pl2.setStaticLight(true);
	}
	
	
	public void spawn(float x, float y,float isize,float itime){
		pl1.setActive(true);
		pl1.setPosition(x, y);
		pl2.setActive(true);
		pl2.setPosition(x, y);
		time=itime;
		TTL=time;
		size=isize;
	}
	
	public static void spawn_one(Vector2 vector,float isize,float itime){
		spawn_one(vector.x,vector.y,isize,itime);
		
	}
	
	public static void spawn_one(Vector2 vector,float isize){
		spawn_one(vector.x,vector.y,isize,.3f);
		
	}
	
	public static void spawn_one(float x, float y,float isize){
		spawn_one(x,y,isize,.3f);
	}
	
	public static void spawn_one(float x, float y,float isize,float itime){
		findOne:
			for (int i = 0; i < explosions.length; i++) {
				if(explosions[i].TTL==0){

					explosions[i].spawn(x, y,isize,itime);

					break findOne;
				}
			}
		
	}
	
	
	
	
	public void upd(float dt){
		
		pl1.setDistance(size*(6-(float)Math.pow(1-((TTL)/time),.1f)*2));
		pl2.setDistance(size*(float)Math.pow(1-(TTL/time),.5f)*30);

		pl1.setColor(.8f, .8f, 1, (float)Math.pow(TTL/(time),3));
		pl2.setColor(.4f, .4f, .4f, TTL/(2*time));

		
		TTL=TTL-dt;
		if(TTL<0){
			TTL=0;
			pl1.setActive(false);
			pl2.setActive(false);

			
		}
	}
	
	public static void ini(){
		explosions = new Explosion[nex];
		for (int i = 0; i < explosions.length; i++) {
			explosions[i] = new Explosion();
		}
		
	}
	
	public static void upd_all(float dt){
		
		for (int i = 0; i < explosions.length; i++) {
			if(explosions[i].TTL>0){
				explosions[i].upd(dt);
			}
		}
		
	}

}
