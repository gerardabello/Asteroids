package com.gerardas.asteroidsDesktop;

import java.security.spec.MGF1ParameterSpec;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;



public class MainMenuScreen implements Screen {
	public Game Mgame;


	private Matrix4 matrix = new Matrix4();
	private float tempscale;

	private com.badlogic.gdx.graphics.OrthographicCamera camera;


	private enum fases{ANIMACIO,FET};
	private fases fase;
	private float dtAnimacio;


	private botoMenu[] botons;
	private final int nBotons=4;

	//mides iguals que al joc, simplement per fer-ho senzill


	private cosAsteroid[] asteroids;



	public SpriteBatch batch;

	private Mesh blackBackMesh;


	private boolean highscoreS;


	private static Texture UiTexture;





	public MainMenuScreen(Game igame) {
		fase=fases.ANIMACIO;
		dtAnimacio=0;

		highscoreS=false;

		batch=new SpriteBatch();

		botoMenu.fontB = new BitmapFont(Gdx.files.internal("data/font/future2G.fnt"),Gdx.files.internal("data/font/future2G.png"),false);
		botoMenu.font = new BitmapFont(Gdx.files.internal("data/font/futureG.fnt"),Gdx.files.internal("data/font/futureG.png"),false);


		Asteroid.ini();
		asteroids = new cosAsteroid[2];
		//TODO set to number of styles
		for (int i = 0; i < asteroids.length; i++) {
			asteroids[i]=new cosAsteroid(i);
		}


		camera=new OrthographicCamera(Global.viewportW,Global.viewportH);

		Mgame = igame;

		UiTexture=new Texture(Gdx.files.internal("data/img/UI.png"));

		set_botons();

		botons[0].visible=true;
		botons[1].visible=true;
		botons[2].visible=true;


		blackBackMesh = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));


		blackBackMesh.setVertices(new float[] { 
				-Global.viewportW/2,  Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, 110),
				-Global.viewportW/2, -Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, 110),
				Global.viewportW/2 , -Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, 110),
				Global.viewportW/2 ,  Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, 110)});



	}


	private void set_botons() {
		botons = new botoMenu[nBotons];

		for (int i = 0; i < botons.length; i++) {
			botons[i] = new botoMenu();
		}

		botons[0].ini(-camera.viewportWidth/2, camera.viewportHeight/4, 24, "LAUNCH");
		botons[1].ini(-camera.viewportWidth/2, botons[0].y-((botons[0].h+botons[1].h)/2+4), 12, "HIGHSCORES");
		botons[2].ini(-camera.viewportWidth/2, botons[1].y-((botons[1].h+botons[2].h)/2+4), 12, "OPTIONS");
		

		botons[3].ini(-camera.viewportWidth/2, -camera.viewportHeight/3, 12, "Return");



	}


	@Override
	public void render (float deltaTime) {

		
		if(Gdx.input.justTouched()){
			for (int i = 0; i < botons.length; i++) {
				if(botons[i].collide(Global.batch2box_x(Gdx.input.getX()), Global.batch2box_y(Gdx.graphics.getHeight()-Gdx.input.getY()))){
					if(i==0){
						Main.newGame();
					}else if(i==1){
						highscoreS=true;
						botoMenu.reset(botons);
						botons[3].visible=true;
					}else if(i==3){
						highscoreS=false;
						botoMenu.reset(botons);
						botons[0].visible=true;
						botons[1].visible=true;
						botons[2].visible=true;
					}
				}
			}
		}



		if(fase==fases.ANIMACIO){
			if(dtAnimacio>=1){
				fase=fases.FET;
				dtAnimacio=1;
				botoMenu.upd_all(botons,dtAnimacio);
			}else if(dtAnimacio<1){

				botoMenu.upd_all(botons,dtAnimacio);

				dtAnimacio+=deltaTime;
			}
		}







		Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
		Gdx.graphics.getGL11().glTexParameterf( GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT );
		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		camera.apply(Gdx.gl11);



		render_cosmeticAsteroid();

		render_black();


		matrix.idt();
		batch.setTransformMatrix(matrix);
		
		UiTexture.bind();

		botoMenu.draw_all(botons,batch);

		if(!highscoreS){


			tempscale=Gdx.graphics.getHeight()/ botoMenu.fontB.getBounds("ASTEDROIDS").width;
			matrix.rotate(0, 0, 1, 90);
			matrix.scale(tempscale, tempscale, tempscale);
			batch.setTransformMatrix(matrix);
			botoMenu.fontB.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			batch.begin();

			botoMenu.fontB.draw(batch, "ASTEDROIDS", 4,-Gdx.graphics.getWidth()/tempscale+botoMenu.fontB.getCapHeight());

			batch.end();
			botoMenu.fontB.getRegion().getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		}else{
			camera.apply(Gdx.gl11);

			Gdx.graphics.getGL11().glTranslatef(Global.viewportW/3, 0, 0);
			//Gdx.graphics.getGL11().glScalef(0.9f, 0.9f, 0);
			render_black();
			Gdx.graphics.getGL11().glTranslatef(-Global.viewportW/3, 0, 0);
			
			botoMenu.font.setScale(Gdx.graphics.getHeight()/480.0f);
			batch.begin();

			botoMenu.font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

			for (int i = 0; i < Highscores.noms.length; i++) {
				if(Highscores.punts[i]!=0){
				botoMenu.font.draw(batch,Highscores.noms[i] , 25+Gdx.graphics.getWidth()/3, Gdx.graphics.getHeight()-botoMenu.font.getCapHeight()*i);
				botoMenu.font.draw(batch,Integer.toString(Highscores.punts[i]) , 25+Gdx.graphics.getWidth()/3+Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()-botoMenu.font.getCapHeight()*i);
				}
			}

			batch.end();
			botoMenu.font.setScale(1);
		}




	}

	private void render_black() {
		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);


		blackBackMesh.render(GL11.GL_TRIANGLE_FAN, 0, 4);

	}


	private void render_cosmeticAsteroid() {
		Gdx.graphics.getGL11().glPushMatrix();


		Gdx.graphics.getGL11().glTranslatef(asteroids[0].pos.x, asteroids[0].pos.y, 0);
		Gdx.graphics.getGL11().glRotatef(	asteroids[0].angle, 0, 0, 1f);


		asteroids[0].draw();


		Gdx.graphics.getGL11().glPopMatrix();


	}


	@Override
	public void pause () {

	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
}


class botoMenu{
	public float h,w,x,y;
	public float realX;
	public String text;
	public boolean visible;

	public Mesh m,mb;
	private float tempscale;

	public static BitmapFont fontB,font;


	public void ini(float ix, float iy,float iw, String iText){


		visible=false;

		x=ix;
		y=iy;
		w=iw;
		h=w*0.45454545f;
		realX=x-2*w;
		text=iText;

		m = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"),
				new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));

		m.setVertices(new float[] { 
				0,  h/2, 0, Color.toFloatBits(255, 255, 255, 255),0,0,
				0, -h/2, 0, Color.toFloatBits(255, 255, 255, 255),0,0.3125f,
				w , -h/2, 0, Color.toFloatBits(255, 255, 255, 255),0.6875f,0.3125f,
				w ,  h/2, 0, Color.toFloatBits(255, 255, 255, 255),0.6875f,0});


		mb = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"),
				new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));

		mb.setVertices(new float[] { 
				0,  h/2, 0, Color.toFloatBits(255, 255, 255, 255),0,0.3125f,
				0, -h/2, 0, Color.toFloatBits(255, 255, 255, 255),0,0.625f,
				w , -h/2, 0, Color.toFloatBits(255, 255, 255, 255),0.6875f,0.625f,
				w ,  h/2, 0, Color.toFloatBits(255, 255, 255, 255),0.6875f,0.3125f});

	}

	public static void reset(botoMenu[] b) {
		for (int i = 0; i < b.length; i++) {
			b[i].visible=false;
		}
	}

	public static void draw_all_blur(botoMenu[] b) {
		Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);


		for (int i = 0; i < b.length; i++) {
			if(b[i].visible)b[i].draw_blur();
		}

		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

	}


	private void draw_blur() {
		Gdx.graphics.getGL11().glPushMatrix();


		Gdx.graphics.getGL11().glTranslatef(realX, y, 0);

		mb.render(GL11.GL_TRIANGLE_FAN, 0, 4);


		Gdx.graphics.getGL11().glPopMatrix();	

	}


	public static void upd_all(botoMenu[] b,float dt) {
		b[2].visible=false;
		for (int i = 0; i < b.length; i++) {
			b[i].upd(dt);
		}		
	}
	private void upd(float dt) {
		realX=x-2*w*(1-dt);	
	}
	public static void draw_all(botoMenu[] b,SpriteBatch batch){
		Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);


		for (int i = 0; i < b.length; i++) {
			if(b[i].visible)b[i].draw();
		}

		botoMenu.draw_all_blur(b);

		botoMenu.draw_all_text(b,batch);

	}


	public static void draw_all_text(botoMenu[] b,SpriteBatch batch) {


		batch.begin();

		for (int i = 0; i < b.length; i++) {
			if(b[i].visible)b[i].draw_text(batch);
		}

		batch.end();
		

	}

	private void draw_text(SpriteBatch batch) {


		if(w>20){
			fontB.setScale(Global.scale*2f);
		}else{
			fontB.setScale(Global.scale*1f);
		}
		

		botoMenu.fontB.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

		fontB.draw(batch, text, Global.box2batch_x(realX)+(float)Math.sqrt(w*4)*Global.scale, Global.box2batch_y(y-h/4)+fontB.getCapHeight());



	}


	public boolean collide(float mx, float my){
		Gdx.app.log("mx", Float.toString(mx));
		Gdx.app.log("my", Float.toString(my));

		return (mx > x && mx<x+w && my > y-h/2 && my<y+h/2) && visible;
	}

	public void draw(){
		Gdx.graphics.getGL11().glPushMatrix();


		Gdx.graphics.getGL11().glTranslatef(realX, y, 0);

		m.render(GL11.GL_TRIANGLE_FAN, 0, 4);


		Gdx.graphics.getGL11().glPopMatrix();		
	}
}



class cosAsteroid{

	Asteroid ast;
	int tipus;
	Vector2 pos;
	float angle;

	public cosAsteroid(int iN){

		//		ast=new Asteroid();
		//		ast.create(2.5f, new Vector2(0,0), new Vector2(0,0), iN,false);
		//		tipus=iN;
		//		pos=new Vector2();
		//		pos.x=Global.viewportW/2-Global.viewportH/6;
		//		pos.y=Global.viewportH/2-Global.viewportH/8-(Global.viewportH/4)*iN;



		ast=new Asteroid();
		ast.create(1.3f*(float)Math.sqrt(Math.pow(Global.viewportW/2f,2)+Math.pow(Global.viewportH/2f,2)), new Vector2(0,0), new Vector2(0,0), iN,false);
		tipus=iN;
		pos=new Vector2();
		pos.x=-Global.viewportW/10;
		pos.y=0;


	}

	public void draw() {
		angle+=Gdx.graphics.getDeltaTime()*3;
		switch (tipus) {
		case 0: 

			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
			Asteroid.tex.bind();
			ast.a_draw_tex();
			break;


		case 1: 
			Gdx.graphics.getGL10().glLineWidth(4);
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			ast.a_draw_glow();
			break;


		}

	}

}

