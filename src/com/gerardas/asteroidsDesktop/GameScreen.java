package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;



public class GameScreen implements Screen{
	Game game;

	SpriteBatch batch;

	Texture BlurTexture;






	private float speed=1f;
	private float sdDt;

	public float totalTime;
	public float points;


	private Mesh blackBackMesh;



	private boolean failSafe;




	private int doBack;
 
	private int blackA;



















	public GameScreen(Game game) {


		this.game = game;
		batch=new SpriteBatch();



		Input.ini();



		Box.ini();
		AT.ini();
		GameUI.ini();




		doBack=0;

		blackA=0;

		blackBackMesh = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));




		points=0;



	}


	@Override
	public void render(float deltaTime) {


		


		sdDt=deltaTime*speed;
		if(sdDt>0.04f){
			sdDt=0.04f;
			failSafe=true;
		}else{
			failSafe=false;
		}


		totalTime+=deltaTime;

		if(Ship.visible()){
			points+=deltaTime*5;  //you win points by being alive. more points per second as you play
		}
		



		Asteroid.spawner(sdDt);
                
                



		Input.Handle(deltaTime);

		Bullet.upd_all(sdDt);
		Explosion.upd_all(sdDt);
		Asteroid.pe.update(sdDt);
		Asteroid.upd_all(sdDt);
		PowerUp.upd_all(sdDt);
		Ship.upd(sdDt);


		doBack++;
		if(doBack>2){
			Background.upd(sdDt*2);
			doBack=0;
		}



		
		if(Ship.powers[2] && (Ship.powerTimes[2]>3 || ((int)(Main.gameScr.totalTime*10.0))%2==0)){
			Global.estil=Global.estils.GLOW;
		}else{
			Global.estil=Global.estils.TEX;
		}

		
		

		Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);

		Box.render(sdDt);

		Gdx.graphics.getGL10().glLineWidth(4*Global.scale);
		renderer();


			Box.rayHandler.setCombinedMatrix(Box.camera.combined);
			Box.rayHandler.updateAndRender();



		//	batch.setProjectionMatrix(Box.camera.combined);


		batch.begin();
		Asteroid.pe.draw(batch);
		AT.updAndDraw_all(sdDt,batch);
		batch.end();

		Gdx.graphics.getGL11().glLoadIdentity();
		Box.camera.apply(Gdx.gl11);


		if(!Ship.visible()){draw_end();}





		//		batch.setProjectionMatrix(idM);
		//		//UI
		//		batch.begin();
		//
		//		Box.font.draw(batch, "fps: " + Gdx.graphics.getFramesPerSecond(), 0, 20);
		//
		//
		//		batch.end();
		//Gdx.app.log("FPS",Integer.toString(Gdx.graphics.getFramesPerSecond()));


	}


	private void draw_end() {

		if(blackA<150){
			blackA+=Gdx.graphics.getDeltaTime()*150f;


			blackBackMesh.setVertices(new float[] { 
					-Global.viewportW/2,  Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, blackA),
					-Global.viewportW/2, -Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, blackA),
					Global.viewportW/2 , -Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, blackA),
					Global.viewportW/2 ,  Global.viewportH/2, 0, Color.toFloatBits(0, 0, 0, blackA)});
		}

		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);


		blackBackMesh.render(GL11.GL_TRIANGLE_FAN, 0, 4);


		if(blackA>=150){
			batch.begin();
			AT.fontB.setScale(1);
			AT.fontB.draw(batch, Integer.toString((int)points), Global.box2batch_x(0)-AT.fontB.getBounds(Integer.toString((int)points)).width/2, Global.box2batch_y(0)+AT.font.getCapHeight()*2);
			batch.end();

		}

	}


	private void renderer() {









		Gdx.graphics.getGL11().glLoadIdentity();
		Box.camera.apply(Gdx.gl11);
		Gdx.graphics.getGL11().glTexParameterf( GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT );
		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);



		//Crash al Mac
		Background.draw();


		Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);

		Asteroid.draw_all();

		PowerUp.draw_all();

		Bullet.draw_all();


		Ship.draw();


		GameUI.draw();

	}


	@Override
	public void resume() {
		batch=new SpriteBatch();

	}

	@Override
	public void pause() {
		batch.dispose();

		Gdx.app.log("GS", "Paused");

	}

	@Override
	public void dispose() {
		//world.dispose();

	}


	@Override
	public void hide() {

	}


	@Override
	public void resize(int width, int height) {


	}


	@Override
	public void show() {

	}




}
