package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector2;

public class Thumbpad {

   private static final int THUMB_AREA_VERTS = 50;
   private static final int THUMB_PT_VERTS = 15;
   
   private final int radius; // Radius in pixels
   private final Vector2 centre; // Centre position in pixels
   private int screenHeight; // Screen height in pixels
   
   private float angle = 0;
   private float amount = 0;
   private Vector2 distance = new Vector2(0,0);

   private Mesh thumbArea;
   private Mesh thumbPoint;
      
   public Thumbpad(int radius, Vector2 centre, int screenHeight) {
      this.radius = radius;
      this.centre = centre;
      this.screenHeight = screenHeight;
      initialiseShapes();
   }
   
   // Call after touch down or touch dragged
   public void updateFromTouch(float x, float y) {
      distance.x = x - centre.x;
      distance.y = y - centre.y;
      amount = distance.len()/radius;   
      if (amount > 1) { amount = 1; }
      amount *= amount;
      angle = (float)(Math.atan2(distance.y, distance.x));
   }
   
   // Call after touch up
   public void setToZero() {
      amount = 0;
   }
   
   // Angle in radians
   public float getAngle() { return angle; }
   
   // Amount 0-1
   public float getAmount() { return amount; }   
         
   public void initialiseShapes() {

      // Initialise thumb area
      thumbArea = new Mesh( 
            true,
            THUMB_AREA_VERTS,
            THUMB_AREA_VERTS,
            new VertexAttribute(Usage.Position, 3, "position")
      );
      float[] vertices = new float[THUMB_AREA_VERTS*3];
      for (int i=0; i<THUMB_AREA_VERTS; i++) {
         float angle = (float)(1/(float)THUMB_AREA_VERTS * i * Math.PI * 2);
         vertices[i*3] = (float)Math.sin(angle) * radius + centre.x;
         vertices[i*3 +1] = (float)Math.cos(angle) * radius + centre.y;
         vertices[i*3 +2] = 0;
      }
      thumbArea.setVertices(vertices);
      
      // Initialise thumb point
      thumbPoint = new Mesh(
            false,
            THUMB_PT_VERTS,
            THUMB_PT_VERTS,
            new VertexAttribute(Usage.Position, 3, "position")
      );
      vertices = new float[THUMB_PT_VERTS*3];
      for (int i=0; i<THUMB_PT_VERTS; i++) {
         float angle = (float)(1/(float)THUMB_PT_VERTS * i * Math.PI * 2);
         vertices[i*3] = (float)Math.sin(angle) * (radius/5) + centre.x;
         vertices[i*3 +1] = (float)Math.cos(angle) * (radius/5) + centre.y;
         vertices[i*3 +2] = 0;
      }
      thumbPoint.setVertices(vertices);
            
   }
   
   public void render() {
      GL11 gl = Gdx.app.getGraphics().getGL11();
      gl.glColor4f(0.9f, 0.3f, 0.3f, 1.0f);
      thumbArea.render(GL11.GL_LINE_LOOP, 0, THUMB_AREA_VERTS);
      gl.glPushMatrix();
      gl.glTranslatef( 
            amount * radius * (float)Math.cos(angle),
            amount * radius * (float)Math.sin(angle),
            0
      );
      thumbPoint.render(GL11.GL_TRIANGLE_FAN, 0, THUMB_PT_VERTS);
      gl.glPopMatrix();
   }
   
}