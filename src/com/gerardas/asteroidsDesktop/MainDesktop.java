package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.backends.jogl.JoglApplication;

public class MainDesktop {
	public static void main (String[] argv) {
		new JoglApplication(new Main(), "Asteroids",800,480, false);
	}
}
