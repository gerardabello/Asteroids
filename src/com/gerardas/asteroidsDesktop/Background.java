package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector2;


public class Background {

	//private static Vector2[] estrelles;
	private static Vector2[] ve;  //velocitat
	private static float[] ie;  //intensitat
	private static float[] le;  //life
	private static Color[] ce;  //alpha

	private static float temp;
	private static Mesh meshb;

	private static final int nEstrelles = 200;

	private static Texture btex,btexDoodle;
	private static float[] meshFloat;
	private static Mesh mesh;
	
	


	public static void ini(){

		meshFloat = new float[(nEstrelles)*4];
		mesh = new Mesh(true, nEstrelles, nEstrelles, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));




		btex=new Texture(Gdx.files.internal("data/img/back.png"));
		btexDoodle=new Texture(Gdx.files.internal("data/img/back_doodle.png"));



		ie=new float[nEstrelles];
		le=new float[nEstrelles];

		//estrelles = new Vector2[nEstrelles];
		ve = new Vector2[nEstrelles];

		ce= new Color[nEstrelles];

		for (int i = 0; i < nEstrelles; i++) {
			//estrelles[i]=new Vector2(     (-.5f+(float)Math.random())*(Box.camera.viewportWidth), (-.5f+(float)Math.random())*(Box.camera.viewportHeight));
			ie[i]=(float)Math.random()*2;
			le[i]=.5f;
			ve[i]=new Vector2(-.5f+(float)Math.random(),-.5f+(float)Math.random());
			ce[i] =new Color(1-((float)Math.random()*0.2f), 1-((float)Math.random()*0.3f), 1, 0);


			
			meshFloat[((i)*4)] = (-.5f+(float)Math.random())*(Box.camera.viewportWidth);
			meshFloat[((i)*4)+1]=(-.5f+(float)Math.random())*(Box.camera.viewportHeight);
			meshFloat[((i)*4)+2] = 0;
			
			
			meshFloat[((i)*4)+3] = Color.toFloatBits(ce[i].r, ce[i].g, ce[i].b, 0);
			
			
			
		}


		meshb = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"),
				new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));

		meshb.setVertices(new float[] { 
				-Box.camera.viewportWidth/2,  -Box.camera.viewportHeight, 0, Color.toFloatBits(255, 255, 255, 255), 0, 1,
				-Box.camera.viewportWidth/2, Box.camera.viewportHeight, 0, Color.toFloatBits(255, 255, 255, 255), 0, 0,
				Box.camera.viewportWidth/2 , Box.camera.viewportHeight, 0, Color.toFloatBits(255, 255, 255, 255), 1, 0,
				Box.camera.viewportWidth/2 , -Box.camera.viewportHeight, 0, Color.toFloatBits(255, 255, 255, 255), 1, 1 });

		//meshb.setIndices(new short[] { 0, 1, 2 ,3});

	}




	public static void upd( float dt){


		for (int i = 0; i < nEstrelles; i++) {
			//			estrelles[i].x+=(ve[i].x*dt)+estrelles[i].x*(dt/10);
			//			estrelles[i].y+=(ve[i].y*dt)+estrelles[i].y*(dt/10);
			if(le[i]<1)le[i]+=dt/10;
			

			if(!Ship.powers[2]){
				meshFloat[((i)*4)] +=(ve[i].x*dt)+meshFloat[((i)*4)]*(dt/10f);
				meshFloat[((i)*4)+1]+=(ve[i].y*dt)+	meshFloat[((i)*4)+1]*(dt/10f);
			}else{
				meshFloat[((i)*4)] +=(ve[i].x*dt)+meshFloat[((i)*4)]*(dt/2f);
				meshFloat[((i)*4)+1]+=(ve[i].y*dt)+	meshFloat[((i)*4)+1]*(dt/2f);
			}
			
			meshFloat[((i)*4)+2] = 0;
			
			temp=((ve[i].x*ve[i].x+ve[i].y*ve[i].y)*3);
			if(temp>1)temp=1;
			temp=(.2f+temp*0.7f)*le[i];
			
			//meshFloat[((i)*4)+3] = Color.toFloatBits(ce[i].r*255, ce[i].g*255, ce[i].b*255, 0);
			meshFloat[((i)*4)+3] = Color.toFloatBits(ce[i].r, ce[i].g, ce[i].b, temp);
			
			//			meshFloat[((i)*4)+3] = Color.toFloatBits(255, 255, 255, 255);



			if(Math.abs(meshFloat[((i)*4)])>Box.camera.viewportWidth/2 || Math.abs(meshFloat[((i)*4)+1])>Box.camera.viewportHeight/2 ){
				le[i]=0;
				meshFloat[((i)*4)] = (-.5f+(float)Math.random())*(Box.camera.viewportWidth/2);
				meshFloat[((i)*4)+1]= (-.5f+(float)Math.random())*(Box.camera.viewportHeight/2);
				meshFloat[((i)*4)+2] = 0;
				meshFloat[((i)*4)+3] = Color.toFloatBits(ce[i].r, ce[i].g, ce[i].b, 0);


			}


		}


		
		mesh.setVertices(meshFloat);


	}


	public static void draw(){
		Gdx.graphics.getGL11().glPushMatrix();

		
		Gdx.graphics.getGL11().glPointSize(3*Global.scale);
		switch (Global.estil) {
		case TEX:
			



			Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			btex.bind();
			meshb.render(GL11.GL_TRIANGLE_FAN, 0, 4);
			
			
			
			

			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			mesh.render(GL11.GL_POINTS, 0, nEstrelles);
			//TODO uncoment, it's a bug on the apple JVM
			
			
			



			

			
			break;

		case GLOW:
			
			
		
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			mesh.render(GL11.GL_POINTS, 0, nEstrelles);
			//TODO uncoment, it's a bug on the apple JVM
			
			
			
			
			
			break;
			
			
		case DOODLE:
			



			Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			btexDoodle.bind();
			meshb.render(GL11.GL_TRIANGLE_FAN, 0, 4);


			

			
			break;
		}
		

		
		
		
		
		Gdx.graphics.getGL11().glPopMatrix();

	}

}














//public class Background {
//
//	private static Vector2[] estrelles;
//	private static float[] ve;  //velocitat
//	private static float[] ie;  //intensitat
//	private static float[] le;   //life
//	private static float temp;
//	
//	private static final int nEstrelles = 1000;
//
//
//	public static void ini(){
//
//		ve=new float[nEstrelles];
//		ie=new float[nEstrelles];
//		le=new float[nEstrelles];
//		estrelles = new Vector2[nEstrelles];
//		for (int i = 0; i < estrelles.length; i++) {
//			estrelles[i]=new Vector2(     (-.5f+(float)Math.random())*(Box.camera.viewportWidth), (-.5f+(float)Math.random())*(Box.camera.viewportHeight));
//			ve[i]=(float)Math.random();
//			ie[i]=(float)Math.random()*2;
//			le[i]=0;
//		}
//
//	}
//
//
//	public static void upd( float dt){
//
//
//
//		for (int i = 0; i < estrelles.length; i++) {
//			estrelles[i].mul(1+(dt*ve[i]/5));
//			if(le[i]<1)le[i]+=dt*ve[i];
//			
//			if(Math.abs(estrelles[i].x)>Box.camera.viewportWidth/2 || Math.abs(estrelles[i].y)>Box.camera.viewportHeight/2 ){
//				ve[i]=(float)Math.random();
//				ie[i]=(float)Math.random();
//				le[i]=0;
//				estrelles[i].set(     (-.5f+(float)Math.random())*(Box.camera.viewportWidth/2), (-.5f+(float)Math.random())*(Box.camera.viewportHeight/2));
//			}
//			
//			
//		}
//
//
//
//
//	}
//
//
//	public static void draw( ImmediateModeRenderer10 IMR){
//
//		Gdx.graphics.getGL11().glPushMatrix();
//
//		Gdx.graphics.getGL11().glPointSize(2);
//		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
//		IMR.begin(GL11.GL_POINTS);
//
//		for (int i = 0; i < estrelles.length; i++) {
//			temp=le[i]*ie[i];
//			if(temp>1)temp=1;
//			IMR.color(1, 1, 1, temp);
//			IMR.vertex(estrelles[i].x,estrelles[i].y , 0);	
//
//		}
//
//
//		IMR.end();
//
//		Gdx.graphics.getGL11().glPopMatrix();
//
//	}
//
//}
