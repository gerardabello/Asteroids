package com.gerardas.asteroidsDesktop;

import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Ship {

	public static Body cos;
	public static Mesh mesh,meshFoc;
	public static float[] meshFocFloat;
	public static float lFoc,lmFoc,aFoc;
	//public static Texture tex;
	private static PolygonShape       tempBoxPoly;
	private static BodyDef tempBoxBodyDef;
	public static boolean power=false;
	public static boolean shoot=false;

	public static Vector2 normDir,tangDir;

	private static PointLight pl2;




	public static float health;



	private static int nGuns;
	private static boolean rear;
	public static int rof,rofM;
	private static Texture tex,texDoodle;


	public static boolean[] powers;   //per a poders temporals
	public static float[] powerTimes;
	private static Mesh meshTriangle;
	private static Texture tex_glow;
	private static Mesh meshShield;
	public static final int nPowers=10;


	public static void ini(){

		powers=new boolean[nPowers];
		powerTimes=new float[nPowers];

		for (int i = 0; i < powers.length; i++) {
			powers[i]=false;
			powerTimes[i]=0;
		}


		rof=3;
		rofM=1;

		normDir=new Vector2();
		tangDir=new Vector2();

		pl2 = new PointLight(Box.rayHandler, 10, new Color(.29f, .68f, 1,.55f), 2.5f, 0, 0);
		pl2.setXray(true);
		pl2.setActive(false);
		//TODO point light del foc


		tex = new Texture(Gdx.files.internal("data/img/ship.png"));
		texDoodle = new Texture(Gdx.files.internal("data/img/ship_doodle.png"));

		tex_glow = new Texture(Gdx.files.internal("data/img/ship_glow.png"));
		tempBoxPoly = new PolygonShape();
		tempBoxBodyDef = new BodyDef();


		tempBoxPoly.set(new Vector2[] {
				new Vector2(-.1f, .8f),
				new Vector2(-.8f, -.8f),
				new Vector2(.8f, -.8f),
				new Vector2(.1f, .8f),
		});

		tempBoxBodyDef.type = BodyType.DynamicBody;
		tempBoxBodyDef.position.x = 0;
		tempBoxBodyDef.position.y = 0;


		meshFocFloat=new float[4*8];

		mesh = new Mesh(true, 4, 4, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"),
				new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		
		mesh.setVertices(new float[] { 
				-.8f,  .8f, 0, Color.toFloatBits(255, 255, 255, 255),0,0,
				-.8f, -.8f, 0, Color.toFloatBits(255, 255, 255, 255),0,1,
				.8f , -.8f, 0, Color.toFloatBits(255, 255, 255, 255),1,1,
				.8f ,  .8f, 0, Color.toFloatBits(255, 255, 255, 255),1,0});

		mesh.setIndices(new short[] { 0, 1, 2 ,3});

		
		
		
		meshTriangle = new Mesh(true, 8, 8, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshTriangle.setVertices(new float[] { 
				-.1f,  .8f, 0, Color.toFloatBits(255, 210, 50, 255),
				-.8f, -.8f, 0, Color.toFloatBits(255, 210, 50, 255),
				-.8f, -.8f, 0, Color.toFloatBits(255, 210, 50, 255),
				.8f , -.8f, 0, Color.toFloatBits(255, 210, 50, 255),
				.8f , -.8f, 0, Color.toFloatBits(255, 210, 50, 255),
				.1f ,  .8f, 0, Color.toFloatBits(255, 210, 50, 255),
				.1f ,  .8f, 0, Color.toFloatBits(255, 210, 50, 255),
				-.1f,  .8f, 0, Color.toFloatBits(255, 210, 50, 255)});


		
		


		meshFoc = new Mesh(true, 8, 8, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));

		meshFocFloat=new float[4*8];
		//meshFoc.setIndices(new short[] { 0, 1, 2 ,3,4,5,6,7});


		meshShield = new Mesh(true, 17, 18, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
		
		meshShield.setVertices(new float[] { 
				0,  0, 0, Color.toFloatBits(74, 174, 255, 2),
				(float)Math.cos(0/16.0*Math.PI*2), (float)Math.sin(0/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(1/16.0*Math.PI*2), (float)Math.sin(1/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(2/16.0*Math.PI*2), (float)Math.sin(2/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(3/16.0*Math.PI*2), (float)Math.sin(3/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(4/16.0*Math.PI*2), (float)Math.sin(4/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(5/16.0*Math.PI*2), (float)Math.sin(5/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(6/16.0*Math.PI*2), (float)Math.sin(6/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(7/16.0*Math.PI*2), (float)Math.sin(7/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(8/16.0*Math.PI*2), (float)Math.sin(8/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(9/16.0*Math.PI*2), (float)Math.sin(9/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(10/16.0*Math.PI*2), (float)Math.sin(10/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(11/16.0*Math.PI*2), (float)Math.sin(11/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(12/16.0*Math.PI*2), (float)Math.sin(12/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(13/16.0*Math.PI*2), (float)Math.sin(13/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(14/16.0*Math.PI*2), (float)Math.sin(14/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150),
				(float)Math.cos(15/16.0*Math.PI*2), (float)Math.sin(15/16.0*Math.PI*2), 0, Color.toFloatBits(74, 174, 255, 150)				});
		
		meshShield.setIndices(new short[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1    });



		cos =Box.world.createBody(tempBoxBodyDef);

		cos.createFixture(tempBoxPoly, 1);
		cos.setUserData("ship");

		tempBoxPoly.dispose();





		nGuns=1;
		health=1;
		rear=false;

	}

	public static void upd_mesh_foc(){
		lFoc=.7f+.4f*(float)Math.random();
		aFoc=.6f+.4f*(float)Math.random();

		meshFocFloat[((0)*4)] = 0;
		meshFocFloat[((0)*4)+1] = -.8f;
		meshFocFloat[((0)*4)+2] = 0;
		meshFocFloat[((0)*4)+3] = Color.toFloatBits(.34f, .78f, 1,aFoc);

		for (int i = 1; i <8 ; i++) {

			meshFocFloat[((i)*4)] = -.6f+(i-1)*.2f;
			meshFocFloat[((i)*4)+1] = -.8f-(float)Math.pow(.6f-Math.abs(meshFocFloat[((i)*4)]),2)*lFoc*3;
			meshFocFloat[((i)*4)+2] = 0;
			meshFocFloat[((i)*4)+3] = Color.toFloatBits(.29f, .68f, 1, aFoc);


		}


		meshFoc.setVertices(meshFocFloat);

	}


	public static void draw_lines(){




		if(power){
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			upd_mesh_foc();
			meshFoc.render(GL11.GL_TRIANGLE_FAN, 0, 8);


		}
		



		switch (Global.estil) {
		case TEX:
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			mesh.render(GL11.GL_TRIANGLE_FAN, 0, 4);
			break;
			
			
		case GLOW:
			Gdx.graphics.getGL11().glDisable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			meshTriangle.render(GL11.GL_LINES, 0, 8);
			
			
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glScalef(3, 3, 0);   //Scalem pk la textura es petita pk te blur.
		
			mesh.render(GL11.GL_TRIANGLE_FAN, 0, 4);
			Gdx.graphics.getGL11().glScalef(1/3, 1/3, 0); 
			
			break;
			
		case DOODLE:
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
			Gdx.graphics.getGL11().glEnable(GL11.GL_TEXTURE_2D);
			mesh.render(GL11.GL_TRIANGLE_FAN, 0, 4);
			break;

		}
		

		if(powers[0]){
			if(powerTimes[0]>3 || ((int)(Main.gameScr.totalTime*10.0))%2==0){
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glScalef(1.5f, 1.5f, 0); 
			Gdx.graphics.getGL11().glTranslatef(0, -0.1f, 0);

			meshShield.render(GL11.GL_TRIANGLE_FAN, 0, meshShield.getNumIndices());
			}
		}


	}


	public static void upd(float dt){
		if(health<=0 && visible()){
	destroy();

		}


		for (int i = 0; i < nPowers; i++) {
			if(powers[i]){
				powerTimes[i]-=dt;
				if(powerTimes[i]<=0){
					powers[i]=false;
				}
			}
		}
		
		


		if(powers[1]){
			rofM=3;
		}else{
			rofM=1;
		}


	}





	private static void destroy() {
		health=0;
		
		Vector3 tempV = new Vector3(cos.getPosition().x, cos.getPosition().y, 0);
		
		Box.camera.project(tempV);

		Explosion.spawn_one(cos.getPosition(), 15,1.6f);
		Asteroid.pe.setPosition(tempV.x, tempV.y);
		Asteroid.pe.getEmitters().get(0).getVelocity().setHighMax( 1500 );
		Asteroid.pe.getEmitters().get(0).getVelocity().setHighMin(200);
		Asteroid.pe.start();
		
		
		set_visible(false);		
		
		Highscores.add("PLAYER", (int)Main.gameScr.points);
	}

	public static void draw(){
		
		switch (Global.estil) {
		case TEX:
			tex.bind();
			break;

		case GLOW:
			Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			tex_glow.bind();
			break;
			
		case DOODLE:
			texDoodle.bind();
			break;
		}
		




		if(visible()){

			if(power){
				pl2.setActive(true);
				pl2.setPosition(cos.getPosition().x-1*(float)Math.sin(-Ship.cos.getAngle()), cos.getPosition().y-1*(float)Math.cos(-Ship.cos.getAngle()));

			}else{
				pl2.setActive(false);
			}
			Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
			Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);


			Gdx.graphics.getGL11().glPushMatrix();
			Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y, 0);
			Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);

			draw_lines();
			Gdx.graphics.getGL11().glPopMatrix();


			//mirror
			if(cos.getPosition().y>Box.camera.viewportHeight*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y-Box.camera.viewportHeight, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				draw_lines();
				Gdx.graphics.getGL11().glPopMatrix();
			}

			if(cos.getPosition().y<-Box.camera.viewportHeight*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x, cos.getPosition().y+Box.camera.viewportHeight, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				draw_lines();
				Gdx.graphics.getGL11().glPopMatrix();
			}


			if(cos.getPosition().x>Box.camera.viewportWidth*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x-Box.camera.viewportWidth, cos.getPosition().y, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				draw_lines();
				Gdx.graphics.getGL11().glPopMatrix();
			}

			if(cos.getPosition().x<-Box.camera.viewportWidth*0.8f/2){
				Gdx.graphics.getGL11().glPushMatrix();
				Gdx.graphics.getGL11().glTranslatef(cos.getPosition().x+Box.camera.viewportWidth, cos.getPosition().y, 0);
				Gdx.graphics.getGL11().glRotatef(cos.getAngle()*360/((float)Math.PI*2), 0, 0, 1f);
				draw_lines();
				Gdx.graphics.getGL11().glPopMatrix();
			}


		}

		Gdx.graphics.getGL11().glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}


	public static void checkBound() {
		if(cos.getPosition().x>Box.camera.viewportWidth/2){
			cos.setTransform(-Box.camera.viewportWidth/2, cos.getPosition().y, cos.getAngle());
		}else if(cos.getPosition().x<-Box.camera.viewportWidth/2){
			cos.setTransform(Box.camera.viewportWidth/2, cos.getPosition().y, cos.getAngle());
		}

		if(cos.getPosition().y>Box.camera.viewportHeight/2){
			cos.setTransform(cos.getPosition().x, -Box.camera.viewportHeight/2, cos.getAngle());
		}else if(cos.getPosition().y<-Box.camera.viewportHeight/2){
			cos.setTransform(cos.getPosition().x, Box.camera.viewportHeight/2, cos.getAngle());
		}


	}

	public static void shoot() {
		if (visible()) {


			normDir.set((float)Math.sin(-Ship.cos.getAngle()), (float)Math.cos(-Ship.cos.getAngle()));
			tangDir.set((float)Math.sin(-Ship.cos.getAngle()+(Math.PI/2)), (float)Math.cos(-Ship.cos.getAngle()+(Math.PI/2)));


			if((nGuns==1 || nGuns==3) || powers[2]){
				Bullet.shoot(Ship.cos.getPosition(), normDir.tmp().mul(25), Ship.cos.getAngle());
			}
			if((nGuns==2 || nGuns==3)|| powers[2]){
				Bullet.shoot(Ship.cos.getPosition().add(tangDir.x/2,tangDir.y/2), normDir.tmp().mul(25), Ship.cos.getAngle());
				Bullet.shoot(Ship.cos.getPosition().sub(tangDir.x/2,tangDir.y/2), normDir.tmp().mul(25), Ship.cos.getAngle());
			}

			if(rear || powers[2]){
				Bullet.shoot(Ship.cos.getPosition(), normDir.tmp().mul(-25), Ship.cos.getAngle());

			}

			//		if(side){
			//			Bullet.shoot(Ship.cos.getPosition(), tangDir.tmp().mul(25), Ship.cos.getAngle()+((float)Math.PI/2));
			//			Bullet.shoot(Ship.cos.getPosition(), tangDir.tmp().mul(-25), Ship.cos.getAngle()-((float)Math.PI/2));
			//
			//		}
		}
	}

	public static boolean visible() {
		return cos.isActive();
	}
	
	private static void set_visible(boolean iv){
		cos.setActive(iv);
	}

	public static void setRear(boolean set) {
		rear=set;
	}

	public static void damage(float hp) {
		if(!powers[0]){
			if(hp<.3){
				health-=hp;
				Gdx.app.log("Damage", Float.toString(hp));
				
			}else{
				health-=.3f;
				Gdx.app.log("Damage", Float.toString(.3f));
			}
		}
		Gdx.app.log("Health", Float.toString(health));
		
	}


	public static void addGun() {
		nGuns++;
		if(nGuns>3)nGuns=3;

	}

	public static void pow_rofM(float itime) {
		powers[1]=true;
		powerTimes[1]=itime;
	}
	
	public static void pow_tw() {
		powers[2]=true;
		powerTimes[2]=20;
		
		pow_rofM(20);
		pow_escut(20);
	}

	public static void pow_escut(float itime) {
		powers[0]=true;
		powerTimes[0]=itime;
	}

	public static int get_rof() {
		return rof*rofM;
	}




}
