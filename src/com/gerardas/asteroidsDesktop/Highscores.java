package com.gerardas.asteroidsDesktop;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class Highscores {
	public static String[] noms;
	public static int[] punts;
	public static final int nHS=10;

	public static Preferences hs;

	public static void ini() {

		noms=new String[nHS];
		punts=new int[nHS];

		hs = Gdx.app.getPreferences("highscores");

		read();

	}


	public static void write() {

		for (int i = 0; i < noms.length; i++) {
			hs.putString("N"+Integer.toString(i),noms[i]);
			hs.putInteger("P"+Integer.toString(i),punts[i]);
		}

		hs.flush();

	}

	public static void read() {

		for (int i = 0; i < noms.length; i++) {
			noms[i]=hs.getString("N"+Integer.toString(i));
			punts[i]=hs.getInteger("P"+Integer.toString(i));
		}

	}

	public static void add(String Nnom, int Npunts) {

		addSearch:
			for (int i = noms.length-1; i >= 0; i--) {
				if(punts[i]<Npunts){
					try{
						punts[i+1]=punts[i];
						noms[i+1]=noms[i];
					}catch (Exception e){
						
					}finally{
						noms[i]=Nnom;
						punts[i]=Npunts;
					}
				}else{
					break addSearch;
				}
			}

	write();

	}



}
