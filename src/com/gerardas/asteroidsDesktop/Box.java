package com.gerardas.asteroidsDesktop;

import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;


public class Box {
	/** the camera **/
	public static com.badlogic.gdx.graphics.OrthographicCamera camera;


	/** box2d debug renderer **/

	/** a spritebatch and a font for text rendering and a Texture to draw our boxes **/

	/** our box2D world **/
	public static World world;



	/** a hit body **/
	static Body hitBody = null;

	public static RayHandler rayHandler;

	public static PointLight pl;












	public static void ini() {






		// setup the camera. In Box2D we operate on a
		// meter scale, pixels won't do it. So we use
		// an orthographic camera with a viewport of
		// 48 meters in width and 32 meters in height.
		// We also position the camera so that it
		// looks at (0,16) (that's where the middle of the
		// screen will be located).
		camera = new OrthographicCamera(48, 48*((float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth()));
		camera.position.set(0, 0, 0);


		// next we create out physics world.
		createPhysicsWorld();

	}

	private static void createPhysicsWorld () {
		// we instantiate a new World with a proper gravity vector
		// and tell it to sleep when possible.
		world = new World(new Vector2(0, 0), true);

		//world.setContinuousPhysics(false);

		createBoxes();

		// You can savely ignore the rest of this method :)
		world.setContactListener(new ContactListener() {

			public  void beginContact (Contact contact) {
				// System.out.println("begin contact");

			}


			public void endContact (Contact contact) {
				// System.out.println("end contact");
			}

			public void preSolve (Contact contact, Manifold oldManifold) {
				// Manifold.ManifoldType type = oldManifold.getType();
				// Vector2 localPoint = oldManifold.getLocalPoint();
				// Vector2 localNormal = oldManifold.getLocalNormal();
				// int pointCount = oldManifold.getPointCount();
				// ManifoldPoint[] points = oldManifold.getPoints();
				// System.out.println("pre solve, " + type +
				// ", point: " + localPoint +
				// ", local normal: " + localNormal +
				// ", #points: " + pointCount +
				// ", [" + points[0] + ", " + points[1] + "]");


				//els bullets i el ship no es poden tocar
				if((contact.getFixtureA().getBody().getUserData().equals("ship") && contact.getFixtureB().getBody().isFixedRotation() ) || (contact.getFixtureB().getBody().getUserData().equals("ship") && contact.getFixtureA().getBody().isFixedRotation() )){
					contact.setEnabled(false);
				}
				
				
				if((contact.getFixtureA().getBody().isFixedRotation() && contact.getFixtureB().getBody().isFixedRotation() )){
					contact.setEnabled(false);
				}

			}


			public void postSolve (Contact contact, ContactImpulse impulse) {
				if(contact.isEnabled()){
					if(contact.getFixtureA().getBody().isFixedRotation()){
						contact.getFixtureA().getBody().setUserData(1);


						Asteroid.apd[Asteroid.apdi]=(Integer)contact.getFixtureB().getBody().getUserData();
						Asteroid.apdi++;
					}
					if(contact.getFixtureB().getBody().isFixedRotation()){
						contact.getFixtureB().getBody().setUserData(1);


						Asteroid.addDestroy((Integer)contact.getFixtureA().getBody().getUserData());

					}



					if(contact.getFixtureA().getBody().getUserData().equals("ship") || contact.getFixtureB().getBody().getUserData().equals("ship")){
						Ship.damage((float)Math.sqrt(impulse.getNormalImpulses()[0]*impulse.getNormalImpulses()[0]+impulse.getNormalImpulses()[1]*impulse.getNormalImpulses()[1])/80f);
					}



				}


				// float[] ni = impulse.getNormalImpulses();
				// float[] ti = impulse.getTangentImpulses();
				// System.out.println("post solve, normal impulses: " + ni[0] + ", " + ni[1] + ", tangent impulses: " + ti[0] + ", " + ti[1]);
			}
		});
	}

	private  static void createBoxes () {


		


		rayHandler = new RayHandler(Box.world);
		rayHandler.setAmbientLight(1, 1, 1, 1);
		rayHandler.setBlur(false);

		
		
		Asteroid.ini();
		Asteroid.create_asteroids();
		Bullet.ini();
		Explosion.ini();
		Ship.ini();
		Background.ini();
		PowerUp.ini();
		
//
//		//creem uns quants asteroides:
//		for (int i = 0; i < 10; i++) {
//			Asteroid.spawn(.8f+(float)Math.random()*1.7f,-24 + (float)(Math.random() * 48), 3 + (float)(Math.random() * 20),-9+(float)Math.random()*18,-9+(float)Math.random()*18);
//		}




	}


	public  static void render(float dt) {
		// first we update the world. For simplicity
		// we use the delta time provided by the Graphics
		// instance. Normally you'll want to fix the time
		// step.

		world.step(dt, 8, 3);

		

		Asteroid.checkDestroy();
		Asteroid.checkBound();
		Ship.checkBound();



	}



	/** we instantiate this vector and the callback here so we don't irritate the GC **/
	Vector3 testPoint = new Vector3();
	QueryCallback callback = new QueryCallback() {

		public  boolean reportFixture (Fixture fixture) {
			// if the hit fixture's body is the ground body
			// we ignore it


			// if the hit point is inside the fixture of the body
			// we report it
			if (fixture.testPoint(testPoint.x, testPoint.y)) {
				hitBody = fixture.getBody();
				return false;
			} else
				return true;
		}
	};






}
