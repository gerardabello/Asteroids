package com.gerardas.asteroidsDesktop;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class Input {


	public static InputProc ip;

	public static float shootDt;

	private static float tempAcc;




	private static Vector2 padPos=new Vector2();
	private static Vector2 finPos=new Vector2();
	private static Vector2 RelfinPos=new Vector2();
	private static int inTurn;

	public static float padPosx,padPosy;

	public static void ini(){
		padPosx=-Global.viewportW/2+6;
		padPosy=-Global.viewportH/2+6;


		padPos.set(Global.box2batch_x(padPosx), Global.box2batch_y(padPosy));

		ip = new InputProc();

	}


	public static void Handle(float dt){


		Ship.power=false;
		Ship.shoot=true;
		//Ship.cos.applyTorque(-((Gdx.input.getAccelerometerY()*Math.abs(Gdx.input.getAccelerometerY())))/1.7f);


		switch (Global.control) {
		case TILT:



			tempAcc=Math.abs(Gdx.input.getAccelerometerY());


			if(tempAcc<1)tempAcc=tempAcc*tempAcc;
			tempAcc=tempAcc*4;
			if(Gdx.input.getAccelerometerY()>0){
				tempAcc=-tempAcc;
			}

			Ship.cos.applyTorque(tempAcc);






			for (int i = 0; i < 5; i++) {


				if(Gdx.input.isTouched(i)){
					if( Gdx.input.getX(i)>=Gdx.graphics.getWidth()/2){
						Ship.shoot=true;

					}
					if(Gdx.input.getX(i)<=Gdx.graphics.getWidth()/2){

						Ship.power=true;

					}


				}

			}

			Ship.cos.applyTorque(-Ship.cos.getAngularVelocity()*5);





			break;


		case DPAD:
			Ship.shoot=true;
			Ship.cos.setAngularVelocity(0);





			for (int i = 0; i < 5; i++) {

				if(Gdx.input.justTouched()){
					finPos.set(Gdx.input.getX(i),Gdx.graphics.getHeight()-Gdx.input.getY(i));
					Gdx.app.log("DST", Double.toString(finPos.dst(Global.box2batch_x(Input.padPosx), Global.box2batch_y(Input.padPosy))));
					if (finPos.dst(Global.box2batch_x(Input.padPosx), Global.box2batch_y(Input.padPosy))<70) {
						inTurn=i;	
					}
				}



				if(Gdx.input.isTouched(i)){
					if( Gdx.input.getX(i)>=Global.box2batch_x(Global.viewportW/2-6) && (Gdx.graphics.getHeight()-Gdx.input.getY(i))<=Global.box2batch_y(-Global.viewportH/2+6)){
						Ship.power=true;




					}else{




					}


				}

			}




			if(inTurn!=99){
				if(Gdx.input.isTouched(inTurn)){
					finPos.set(Gdx.input.getX(inTurn),Gdx.graphics.getHeight()-Gdx.input.getY(inTurn));
					RelfinPos.set(finPos);
					RelfinPos.sub(padPos);
					Ship.cos.setTransform(Ship.cos.getPosition(), (RelfinPos.angle()-90)/360*2*3.14f);


					Gdx.app.log("TOUCH", "TURN:");
					Gdx.app.log("p", Float.toString(Gdx.input.getX(inTurn))+","+Float.toString(Gdx.input.getY(inTurn)));

				}else{
					inTurn=99;
				}

			}



			break;


		case ARROWS:
			Ship.shoot=true;





			for (int i = 0; i < 5; i++) {

	


				if(Gdx.input.isTouched(i)){
					if( Gdx.input.getX(i)>=Global.box2batch_x(Global.viewportW/2-6) && (Gdx.graphics.getHeight()-Gdx.input.getY(i))<=Global.box2batch_y(-Global.viewportH/2+6)){
						Ship.power=true;




					}else if( Gdx.input.getX(i)<=Global.box2batch_x(-Global.viewportW/2+3*GameUI.scale) && (Gdx.graphics.getHeight()-Gdx.input.getY(i))<=Global.box2batch_y(-Global.viewportH/2+6)){
						if(Gdx.input.getX(i)<=Global.box2batch_x(-Global.viewportW/2+(3*GameUI.scale/2))){
							Ship.cos.applyTorque(16);

						}else{
							Ship.cos.applyTorque(-16);

						}



					}


				}

			}
			
			
			
			Ship.cos.applyTorque(-Ship.cos.getAngularVelocity()*5);


			break;

		}		

		
		
		
		
		
		
		if(Gdx.input.isKeyPressed(Keys.W)){
			Ship.power=true;
		}

		if(Gdx.input.isKeyPressed(Keys.D)){
			Ship.cos.applyTorque(-16);
		}else if(Gdx.input.isKeyPressed(Keys.A)){
			Ship.cos.applyTorque(16);
		}
		
		
		
		
		
		
		
		

		if(Ship.power){
			Ship.cos.applyForceToCenter(15*(float)Math.sin(-Ship.cos.getAngle()), 15*(float)Math.cos(-Ship.cos.getAngle()));

		}




		if(Ship.shoot){
			shootDt+=dt;
			if (shootDt>=(1f/Ship.get_rof())) {
				shootDt=0;
				Ship.shoot();

			}
		}else{
			if(shootDt<(1f/Ship.get_rof())){
				shootDt+=dt;
			}else{
				shootDt=(1f/Ship.get_rof());
			}
		}

	}

}