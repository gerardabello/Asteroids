package com.gerardas.asteroidsDesktop;



import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector2;

public class PowerUp {

	private static PowerUp[] powers;
	private static final int Npowers = 70;

	private static final int Nvertex = 6;
	private static final float radi = .9f;

	private Mesh mesh;
	private static float[] meshFloat;
	private static float tempA;

	private Vector2 pos,vel;
	private boolean visible;
	private float alpha,life;
	private int tipus; //1: armes(blau)   2:vides/proteccio(verd)    3:bomba nuclear(groc)    4:SUPER (red)
	private PointLight pl;


	private PowerUp(){

		vel=new Vector2(0,0);
		pos=new Vector2(0,0);
		visible=false;
		alpha=1;
		life=0;


		tempA=(float)Math.random();
		if(tempA>.5){
			if(tempA>.96){
				tipus=3;
			}else if (tempA>.92){
				tipus=4;
			}else{
				tipus=1;
			}
		}else{
			tipus=2;
		}



		mesh = new Mesh(true, Nvertex, Nvertex, 
				new VertexAttribute(Usage.Position, 3, "a_position"),
				new VertexAttribute(Usage.ColorPacked, 4, "a_color"));

		pl = new PointLight(Box.rayHandler, 6, new Color(.6f,.6f,1,.5f), 2, 0, 0);
		pl.setActive(false);
		pl.setXray(true);


		mesh.setVertices(meshFloat);

	}


	private float tColor(float a){

		if(tipus==1){
			pl.setColor(.1f, .6f, 1.0f, a);
			return Color.toFloatBits(.1f, .6f, 1.0f, a);

		}else if(tipus==2){
			pl.setColor(.2f, 1f, .2f, a);
			return Color.toFloatBits(.2f, 1f, .2f, a);
		}else if(tipus==3){
			pl.setColor(1.0f, .8f, .3f, a);
			return Color.toFloatBits(1.0f, .8f, .3f, a);
		}else if(tipus==4){
			pl.setColor(1.0f, .2f, 0f, a);
			return Color.toFloatBits(1.0f, .2f, 0f, a);
		}else{
			pl.setColor(1.0f, 0, 0, a);
			return Color.toFloatBits(1.0f, 0, 0, a);
		}
	}


	public static void ini(){




		meshFloat = new float[(Nvertex)*4];

		tempA=0;

		meshFloat[0]=radi;
		meshFloat[1]=0;	
		meshFloat[2]=0;
		meshFloat[3]=Color.toFloatBits(1.0f, 1.0f, 1.0f, 1.0f);


		for (int i = 4; i < meshFloat.length; i=i+4) {
			tempA=(float)(2.0f*Math.PI/Nvertex)*(i/4.0f);

			meshFloat[i+0]=(float)Math.cos(tempA)*radi;
			meshFloat[i+1]=(float)Math.sin(tempA)*radi;
			meshFloat[i+2]=0;
			meshFloat[i+3]=Color.toFloatBits(1.0f, 1.0f, 1.0f, 1.0f);

		}

		powers = new PowerUp[Npowers];
		for (int i = 0; i < powers.length; i++) {
			powers[i] = new PowerUp();
		}


	}


	public void draw(){
		Gdx.graphics.getGL11().glTranslatef(pos.x,pos.y, 0);
		mesh.render(GL11.GL_TRIANGLE_FAN,0,Nvertex);
	}


	public static void draw_all(){
		Gdx.graphics.getGL11().glDisable(GL11.GL_TEXTURE_2D);
		Gdx.graphics.getGL11().glEnable(GL11.GL_BLEND);
		for (int i = 0; i < powers.length; i++) {
			if(powers[i].visible){
				Gdx.graphics.getGL11().glPushMatrix();
				powers[i].draw();
				Gdx.graphics.getGL11().glPopMatrix();
			}
		}
	}


	public static void spawn_one(Vector2 ipos, Vector2 ivel){
		findOne:
			for (int i = (int)(Math.random()*Npowers/1.5f); i < powers.length; i++) {
				if(!powers[i].visible){
					if(!((powers[i].tipus==4 || powers[i].tipus==4) && Main.gameScr.totalTime<90)){    // les de tipus 4 i 3 nomes surten a partir de 90 segons
						powers[i].spawn(ipos, ivel);
						break findOne;
					}
				}
			}
	}


	private void spawn(Vector2 ipos, Vector2 ivel) {
		visible=true;
		pos.set(ipos);
		vel.set(ivel);
		alpha=1;
		life=0;
		pl.setActive(true);

	}



	public void upd(float dt){
		pos.x+=vel.x*dt;
		pos.y+=vel.y*dt;
		life+=dt;
		if(pos.dst(Ship.cos.getPosition())<1.8f){
			collision();
		}
		checkBound();

		tempA=(.7f)+(float)Math.cos(life*5.0f)*(.25f);
		if(life>16){
			tempA=tempA*(17.0f-life);
			if(life>17){
				destroy();
			}
		}
		for (int i = 0; i < meshFloat.length; i=i+4) {
			meshFloat[i+3]=tColor(tempA);
		}



		mesh.setVertices(meshFloat);
		pl.setPosition(pos);
	}





	public void checkBound() {
		if(pos.x>Box.camera.viewportWidth/1.85f){
			pos.set(-Box.camera.viewportWidth/1.85f, pos.y);
		}else if(pos.x<-Box.camera.viewportWidth/1.85f){
			pos.set(Box.camera.viewportWidth/1.85f, pos.y);
		}

		if(pos.y>Box.camera.viewportHeight/1.85f){
			pos.set(pos.x, -Box.camera.viewportHeight/1.85f);
		}else if(pos.y<-Box.camera.viewportHeight/1.85f){
			pos.set(pos.x, Box.camera.viewportHeight/1.85f);
		}

	}


	private void destroy() {

		visible=false;
		pl.setActive(false);

	}


	private void collision() {



		if(tipus==1){
			tempA=(float)Math.random();
			if(tempA<=.15){
				Ship.setRear(true);
				AT.spawn_one(pos, "REAR GUN");
			}else if(tempA<=.5){

				Ship.addGun();
				AT.spawn_one(pos, "NEW GUN");

			}else{
				Ship.pow_rofM(10);
				AT.spawn_one(pos, "GUN SPEED (10s)");

			}

		}else if(tipus==2){

			tempA=(float)Math.random();
			if(tempA<=.5){

				Ship.health+=.4f;
				if(Ship.health>1)Ship.health=1;
				AT.spawn_one(pos, "HEALTH");

			}else{
				Ship.pow_escut(10);
				AT.spawn_one(pos, "SHIELD (10s)");

			}


		}else if(tipus==3){
			//nuclear bomb
			AT.spawn_one(pos, "NUCLEAR BOMB");
			Explosion.spawn_one(pos, 60, 4f);
			for (int j = 0; j < 2; j++) {
				for (int i = 0; i < Asteroid.asteroids.length; i++) {
					if(Asteroid.asteroids[i].visible){
						Asteroid.asteroids[i].destroy();
					}
				}
			}

		}else if(tipus==4){
			//super
			AT.spawn_one(pos, "TIMEWARP");
			Explosion.spawn_one(pos, 60, 4f);
			Ship.pow_tw();


		}

		destroy();

	}



	public static void upd_all(float dt){

		for (int i = 0; i < powers.length; i++) {
			if(powers[i].visible){
				powers[i].upd(dt);
			}

		}

	}



}
