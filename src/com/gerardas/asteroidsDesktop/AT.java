package com.gerardas.asteroidsDesktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


//AWESOME TEXT
public class AT {



	public static AT[] ATs;
	public static BitmapFont font,fontB;
	public static final int nAT=5;
	public static final float time=2f;




	public float TTL;
	public String text;
	public Vector2 pos;
	public int scale;
	

	public AT(){
		scale=1;
		TTL=0;
		text="";
		pos=new Vector2();
	}


	public void spawn(float x, float y,String itext, int iScale){
		scale=iScale;
		text=itext;
		TTL=time;
		pos.set(Global.box2batch_x(x),Global.box2batch_y(y));
		pos.x=pos.x-font.getBounds(itext).width*scale/2;
		pos.y=pos.y+font.getBounds(itext).height;
	}

	public static void spawn_one(Vector2 vector,String itext){
		spawn_one(vector.x,vector.y,itext,1);
	}
	
	public static void spawn_one(Vector2 vector,String itext, int iScale){
		spawn_one(vector.x,vector.y,itext,iScale);
	}
	
	public static void spawn_one(float x, float y,String itext){
		spawn_one(x,y,itext,1);
	}

	public static void spawn_one(float x, float y,String itext, int iScale){
		Gdx.app.log("Spawn", "TEXT");
		findOne:
			for (int i = 0; i < ATs.length; i++) {
				if(ATs[i].TTL==0){

					ATs[i].spawn(x, y,itext,iScale);

					break findOne;
				}
			}
	}




	public void upd(float dt,SpriteBatch batch){
		
		font.setScale(1);
		font.setColor(1.0f, 1.0f, 1.0f, TTL/(time));
		font.draw(batch, text, pos.x, pos.y+(time-TTL)*30);
		
		
		
		TTL=TTL-dt;
		if(TTL<0){
			TTL=0;
		}
	}

	public static void ini(){
		


			font=new BitmapFont(Gdx.files.internal("data/font/futureG.fnt"),Gdx.files.internal("data/font/futureG.png"),false);
			fontB=new BitmapFont(Gdx.files.internal("data/font/future2G.fnt"),Gdx.files.internal("data/font/future2G.png"),false);

			//font=new BitmapFont(Gdx.files.internal("data/font/guix.fnt"),Gdx.files.internal("data/font/guix.png"),false);


		
		ATs = new AT[nAT];
		for (int i = 0; i < ATs.length; i++) {
			ATs[i] = new AT();
		}
		
		


	}

	public static void updAndDraw_all(float dt,SpriteBatch batch){

		for (int i = 0; i < ATs.length; i++) {
			if(ATs[i].TTL>0){
				ATs[i].upd(dt,batch);
			}
		}
		
		if(Asteroid.is_in_field()){
			font.setScale(1);
			font.setColor(1.0f, .7f, .7f, .7f+(float)Math.cos(Main.gameScr.totalTime*6)*.3f);

		//	font.draw(batch, "ASTEROID FIELD", Global.box2batch_x(Box.camera.viewportWidth/2-1-(font.getBounds("ASTEROID FIELD").width*(Box.camera.viewportWidth/Gdx.graphics.getWidth()))), Global.box2batch_y(Box.camera.viewportHeight/2-1));
			font.draw(batch, "ASTEROID FIELD", Global.box2batch_x(0)-font.getBounds("ASTEROID FIELD").width/2, Global.box2batch_y(0)+font.getCapHeight());

		}
		
		
		font.setScale(1);
		font.setColor(1.0f, 1.0f, 1.0f, 1f);
		
		font.draw(batch, Integer.toString((int)Main.gameScr.points), Global.box2batch_x(Box.camera.viewportWidth/2-1-(font.getBounds(Integer.toString((int)Main.gameScr.points)).width*(Box.camera.viewportWidth/Gdx.graphics.getWidth()))), Global.box2batch_y(Box.camera.viewportHeight/2-1)+20);

		

	}

}
